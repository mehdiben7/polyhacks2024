use osmpbfreader::OsmObj;
use poem_openapi::{Enum, Object};

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, PartialOrd, Ord, Enum)]
#[repr(u8)]
pub enum PlaceOfInterest {
    Bank = 0,
    Bar = 1,
    Bicycle = 2,
    CarChargingStation = 3,
    Childcare = 4,
    Cinema = 5,
    Clinic = 6,
    Pharmacy = 7,
    CommunityCenter = 8,
    ConcertHall = 9,
    ConvenienceStore = 10,
    DancingSchool = 11,
    Grocery = 12,
    Library = 13,
    Restaurant = 14,
    Training = 15,
    Muslim = 16,
    Christian = 17,
    Buddhist = 18,
    Jewish = 19,
}

impl PlaceOfInterest {
    pub fn from_str(value: &str, religion: Option<&str>) -> Option<Self> {
        if let Some(religion) = religion {
            return match religion {
                "buddhist" => Some(Self::Buddhist),
                "christian" => Some(Self::Christian),
                "muslim" => Some(Self::Muslim),
                "jewish" => Some(Self::Jewish),
                _ => None,
            };
        }
        match value {
            "bank" => Some(Self::Bank),
            "bar" | "nightclub" => Some(Self::Bar),
            "bicycle" | "bicycle_rental" | "bicycle_repair_station" | "bicycle_trailer_sharing" => {
                Some(Self::Bicycle)
            }
            "charging_station" => Some(Self::CarChargingStation),
            "childcare" | "kindergarten" => Some(Self::Childcare),
            "cinema" => Some(Self::Cinema),
            "clinic" | "doctors" | "hospital" => Some(Self::Clinic),
            "pharmacy" => Some(Self::Pharmacy),
            "community_center" => Some(Self::CommunityCenter),
            "concert_hall" => Some(Self::ConcertHall),
            "convenience_store" => Some(Self::ConvenienceStore),
            "dancing_school" => Some(Self::DancingSchool),
            "grocery" => Some(Self::Library),
            "library" => Some(Self::Restaurant),
            "cafe" | "fast_food" | "feeding_place" | "food_court" | "internet_cafe"
            | "restaurant" | "restaurant;bar" => Some(Self::Restaurant),
            "training" => Some(Self::Training),
            _ => None,
        }
    }
}

#[derive(Clone, Debug, Hash, PartialEq, PartialOrd, Eq, Ord, Object)]
pub struct Tag {
    key: String,
    value: String,
}

impl Tag {
    pub fn from((key, value): (String, String)) -> Self {
        Self { key, value }
    }
}

#[derive(Clone, Debug, Hash, PartialEq, PartialOrd, Eq, Ord, Object)]
pub struct InterestPoint {
    pub longitude: i32,
    pub latitude: i32,
    place_interest: PlaceOfInterest,
    tags: Vec<Tag>,
}

impl InterestPoint {
    pub fn from(obj: OsmObj) -> Option<Self> {
        let OsmObj::Node(node) = obj else { return None };
        let place_interest = PlaceOfInterest::from_str(
            node.tags.get("amenity")?,
            node.tags.get("religion").map(|c| c.as_str()),
        )?;
        let tags = node
            .tags
            .iter()
            .map(|(a, b)| Tag::from((a.to_string(), b.to_string())))
            .collect();
        let latitude = node.decimicro_lat;
        let longitude = node.decimicro_lon;

        Some(Self {
            place_interest,
            tags,
            latitude,
            longitude,
        })
    }

    pub fn new(
        latitude: i32,
        longitude: i32,
        place_interest: PlaceOfInterest,
        tags: Vec<Tag>,
    ) -> Self {
        Self {
            latitude,
            longitude,
            place_interest,
            tags,
        }
    }
    pub fn get_type(&self) -> PlaceOfInterest {
        self.place_interest
    }
    pub fn longitude(&self) -> f64 {
        self.longitude as f64 * 1e-7
    }
    pub fn latitude(&self) -> f64 {
        self.latitude as f64 * 1e-7
    }
}
