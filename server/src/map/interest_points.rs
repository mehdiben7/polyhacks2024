use std::collections::HashSet;

use super::interest_point::InterestPoint;

#[derive(Debug, Default)]
pub struct InterestPoints {
    longitude: Vec<InterestPoint>,
    latitude: Vec<InterestPoint>,
}

const MAX_DISTANCE: f64 = 0.011;

impl InterestPoints {
    pub fn push(&mut self, interest_point: InterestPoint) {
        self.longitude.push(interest_point.clone());
        self.latitude.push(interest_point);
    }
    pub fn sort(&mut self) {
        self.longitude.sort_unstable_by_key(|point| point.longitude);
        self.latitude.sort_unstable_by_key(|point| point.latitude);
    }
    pub fn get(&self, longitude: f64, latitude: f64) -> Vec<InterestPoint> {
        let inf = self.latitude.partition_point(|interest_point| {
            !(interest_point.latitude() > latitude - MAX_DISTANCE)
        });
        let sup = self
            .latitude
            .partition_point(|interest_point| interest_point.latitude() < latitude + MAX_DISTANCE);

        let latitude: HashSet<InterestPoint> = self.latitude[inf..sup].iter().cloned().collect();

        let inf = self.longitude.partition_point(|interest_point| {
            !(interest_point.longitude() > longitude - MAX_DISTANCE)
        });
        let sup = self.longitude.partition_point(|interest_point| {
            interest_point.longitude() < longitude + MAX_DISTANCE
        });

        let longitude: HashSet<InterestPoint> = self.longitude[inf..sup].iter().cloned().collect();
        latitude.intersection(&longitude).cloned().collect()
    }
}
