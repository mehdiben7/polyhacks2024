use self::{
    interest_point::{InterestPoint, PlaceOfInterest},
    interest_points::InterestPoints,
};

pub mod interest_point;
pub mod interest_points;

#[derive(Debug, Default)]
pub struct Map {
    pub interests_point: [InterestPoints; 20],
}

impl Map {
    pub fn push(&mut self, interest_point: InterestPoint) {
        self.interests_point[interest_point.get_type() as usize].push(interest_point);
    }
    pub fn build(&mut self) {
        for interest_point in self.interests_point.iter_mut() {
            interest_point.sort();
        }
    }
    pub fn get(
        &self,
        place_of_interest: PlaceOfInterest,
        longitude: f64,
        latitude: f64,
    ) -> Vec<InterestPoint> {
        self.interests_point[place_of_interest as usize].get(longitude, latitude)
    }
}
