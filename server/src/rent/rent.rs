use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct Rent {
    id: i32,
    number_of_closed_room: i32,
    price: f32,
    latitude: f64,
    longitude: f64,
    full_address: String,
}
