use std::fs;

use super::rent::Rent;

#[derive(Debug)]
pub struct Rents {
    rents: Vec<Rent>,
}

impl Rents {
    pub fn new(filename: &str) -> Self {
        let file = fs::File::open(filename).expect("file should open read only");

        let rents: Vec<Rent> = serde_json::from_reader(file).expect("file should be proper JSON");

        Rents { rents }
    }
}
