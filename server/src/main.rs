use std::{
    fs::File,
    sync::{Arc, Mutex},
};

use poem::{listener::TcpListener, middleware::Cors, EndpointExt, Route, Server};
use poem_openapi::{payload::Json, ApiResponse, Object, OpenApi, OpenApiService};
use server::map::{
    interest_point::{InterestPoint, PlaceOfInterest},
    Map,
};
struct State {
    pub map: Map,
}

struct Api {
    pub state: Arc<Mutex<State>>,
}

#[derive(Object)]
struct ServiceRequestData {
    pub tags: Vec<String>,
    pub latitude: f64,
    pub longitude: f64,
}

#[derive(Object)]
struct ServiceResponseData {
    place_interest: Vec<InterestPoint>,
}

#[derive(ApiResponse)]
enum ServiceResponse {
    #[oai(status = 200)]
    Ok(Json<ServiceResponseData>),
}

#[OpenApi]
impl Api {
    pub fn new() -> Self {
        let r = File::open(&std::path::Path::new("montreal.osm.pbf")).unwrap();

        let mut pbf = osmpbfreader::OsmPbfReader::new(r);
        let mut map = Map::default();
        for obj in pbf.par_iter().map(Result::unwrap) {
            if let Some(interesting_point) = InterestPoint::from(obj) {
                map.push(interesting_point);
            }
        }

        Self {
            state: Arc::new(Mutex::new(State { map })),
        }
    }

    #[oai(path = "/service", method = "post")]
    async fn index(&self, input: Json<ServiceRequestData>) -> ServiceResponse {
        let mut data: Vec<InterestPoint> = vec![];
        for tag in input.tags.iter() {
            let place_of_interest = PlaceOfInterest::from_str(tag.as_str(), None);
            if let Some(place_of_interest) = place_of_interest {
                let d = self.state.lock().unwrap().map.get(
                    place_of_interest,
                    input.longitude,
                    input.latitude,
                );
                data.extend(d.iter().cloned());
            }
        }
        ServiceResponse::Ok(Json(ServiceResponseData {
            place_interest: data,
        }))
    }
}

#[tokio::main]
async fn main() -> Result<(), std::io::Error> {
    if std::env::var_os("RUST_LOG").is_none() {
        std::env::set_var("RUST_LOG", "poem=debug");
    }
    tracing_subscriber::fmt::init();

    let api_service =
        OpenApiService::new(Api::new(), "Rustaheat", "1.0").server("http://0.0.0.0:6942/api");
    let ui = api_service.swagger_ui();

    Server::new(TcpListener::bind("0.0.0.0:6942"))
        .run(
            Route::new()
                .nest("/api", api_service)
                .nest("/", ui)
                .with(Cors::new()),
        )
        .await
}
