use std::{fs::File, time::Instant};

use server::map::{
    interest_point::{InterestPoint, PlaceOfInterest},
    Map,
};

fn main() {
    let r = File::open(&std::path::Path::new("montreal.osm.pbf")).unwrap();

    let mut pbf = osmpbfreader::OsmPbfReader::new(r);
    let mut map = Map::default();
    for obj in pbf.par_iter().map(Result::unwrap) {
        if let Some(interesting_point) = InterestPoint::from(obj) {
            map.push(interesting_point);
        }
    }
    map.build();

    let time = Instant::now();
    println!(
        "{:#?} computed in {}s",
        map.get(PlaceOfInterest::Restaurant, -73.76915, 45.53415),
        time.elapsed().as_secs_f32()
    );
}
