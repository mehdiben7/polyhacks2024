import type { Error } from "$lib/Types/error"
import { HTTPMethods, from } from "$lib/Types/methods"

const API_URL = "http://localhost:6942/api/"

export const get = async <R>(endpoint: string): Promise<R | Error> => {
    return await request(HTTPMethods.GET, endpoint)
}

export const post = async <T, R>(endpoint: string, data: T): Promise<R | Error> => {
    return await request(HTTPMethods.POST, endpoint, data)
}

export const put = async <T, R>(endpoint: string, data: T): Promise<R | Error> => {
    return await request(HTTPMethods.PUT, endpoint, data)
}

export const del = async (endpoint: string): Promise<Error> => {
    return await request(HTTPMethods.DELETE, endpoint)
}

const request = async <T, R>(method: HTTPMethods, endpoint: string, data: undefined | T = undefined): Promise<R | Error> => {
    try {
        switch (method) {
            case HTTPMethods.POST || HTTPMethods.PUT: {
                console.log("data", from(method))
                const res = await fetch(`${API_URL}${endpoint}`, {
                    headers: {
                        "Content-Type": "application/json",
                    },
                    method: from(method),
                    body: JSON.stringify(data)
                })
                if (res.status > 300) {
                    return handleError(res.status)
                }
                return (await res.json()) as R
            }
            case HTTPMethods.DELETE: {
                const res = await fetch(`${API_URL}${endpoint}`, {
                    method: from(method)
                })
                if (res.status > 300) {
                    return handleError(res.status)
                }
                return {} as R
            }
            default: {
                const res = await fetch(`${API_URL}${endpoint}`, {
                    method: from(method)
                })
                if (res.status > 300) {
                    return handleError(res.status)
                }
                return (await res.json()) as R
            }

        }
    } catch (e) {
        return handleError(-1)
    }
}

const ERROR_MSG_ID = "f93f17a0-d14d-4333-af9e-063c4ff324c6"
const error = (msg: string): Error => {
    return { err_message: msg, object_id: ERROR_MSG_ID }
}
export const isError = (result: any): result is Error => {
    return result !== undefined && result.err_message !== undefined && result.object_id !== undefined && result.object_id === ERROR_MSG_ID;
}
const handleError = (statusCode: number): Error => {
    switch (statusCode) {
        default: return error("An unknown error occured")
    }
}