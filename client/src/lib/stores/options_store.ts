import { MapOptions } from '$lib/options';
import { writable } from 'svelte/store';

export const OptionsStore = writable(MapOptions);