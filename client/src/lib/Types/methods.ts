export enum HTTPMethods{
    GET,POST,PUT,DELETE
}

export const from = (method: HTTPMethods): string => {
    switch(method){
        case HTTPMethods.GET: return "GET"
        case HTTPMethods.POST: return "POST"
        case HTTPMethods.PUT: return "PUT"
        case HTTPMethods.DELETE: return "DELETE"
    }
}