export type MapResponse = {
    place_interest: {
        longitude: number;
        latitude: number;
        place_interest: string;
        tags: { key: string, value: string }[]
    }[];
};